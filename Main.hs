{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Main where

import           Control.Concurrent   (threadDelay)
import qualified Control.Exception    as E
import           Control.Lens         ((^.))
import           Control.Monad        (forever)
import           Control.Monad.Reader (ask)
import           Control.Monad.State  (get, put)
import           Crypto.Hash          (SHA1, hash)
import           Data.Acid            (AcidState, Query, Update, closeAcidState,
                                       makeAcidic, openLocalState, query,
                                       update)
import           Data.Aeson           (FromJSON (..))
import qualified Data.Aeson           as Aeson
import           Data.Aeson.Types     (Options (fieldLabelModifier), camelTo2)
import           Data.ByteString      (ByteString)
import qualified Data.ByteString.Lazy as LB
import           Data.Foldable        (forM_)
import           Data.List            (stripPrefix)
import qualified Data.Map.Strict      as Map
import           Data.Maybe           (fromMaybe)
import           Data.Monoid          ((<>))
import           Data.SafeCopy        (Migrate (..), base, deriveSafeCopy,
                                       extension)
import           Data.Text            (Text, pack, unpack)
import           Data.Text.Encoding   (decodeUtf8, encodeUtf8)
import           Data.Time            (UTCTime, getCurrentTime)
import           Data.Typeable        (Typeable)
import           Data.Yaml            as Yaml
import qualified DBus.Notify          as Notify
import           GHC.Generics         (Generic)
import qualified Haquery
import           Network.HTTP.Client  (HttpException)
import qualified Network.Wreq         as Wreq
import           System.Environment   (getArgs)


-- The state

newtype SiteKey = SiteKey Text
  deriving (Eq, Ord, Show, Typeable)

$(deriveSafeCopy 0 'base ''SiteKey)

data SiteDataV0 = SiteDataV0
  { lastChecked :: UTCTime
  , contentHash :: SHA1
  } deriving (Show, Typeable)

$(deriveSafeCopy 0 'base ''SHA1)
$(deriveSafeCopy 0 'base ''SiteDataV0)

data SiteData = SiteData
  { lastChecked :: UTCTime
  , lastChanged :: UTCTime
  , contentHash :: SHA1
  } deriving (Show, Typeable)

instance Migrate SiteData where
  type MigrateFrom SiteData = SiteDataV0
  migrate (SiteDataV0 a b) = SiteData a a b

$(deriveSafeCopy 1 'extension ''SiteData)

type Database = Map.Map SiteKey SiteData

data WatchItState = WatchItState Database
  deriving (Show, Typeable)

$(deriveSafeCopy 0 'base ''WatchItState)


-- Transactions to execute over the state

insertSiteData :: SiteKey -> SiteData -> Update WatchItState ()
insertSiteData key value = do
  WatchItState db <- get
  put (WatchItState (Map.insert key value db))

lookupSiteData :: SiteKey -> Query WatchItState (Maybe SiteData)
lookupSiteData key = do
  WatchItState db <- ask
  return (Map.lookup key db)

getDatabase :: Query WatchItState Database
getDatabase = do
  WatchItState db <- ask
  return db

$(makeAcidic ''WatchItState ['insertSiteData, 'lookupSiteData])

makeSiteKey :: Site -> SiteKey
makeSiteKey site = SiteKey $ siteUrl site <> siteSelector site


-- The application


data Config = Config
  { checkFreqSec :: Int
  , sites :: [Site]
  } deriving (Generic, Show)

instance FromJSON Config where
  parseJSON = Aeson.withObject "Config" $ \v ->
    Config
      <$> v Aeson..:? "check_freq_sec" Aeson..!= (15*60) -- default: 15 mins
      <*> v Aeson..: "sites"


data Site = Site
  { siteUrl :: Text
  , siteSelector :: Text
  } deriving (Generic, Show)

instance FromJSON Site where
  parseJSON =
    Aeson.genericParseJSON $
      Aeson.defaultOptions {
        fieldLabelModifier = \key -> camelTo2 '_' $ fromMaybe key $ stripPrefix "site" key
      }

-- TODO: add arg to delete unneeded records in the database (if you change a selector or remove a site)
main :: IO ()
main = do
  args <- getArgs
  acid <- openLocalState (WatchItState Map.empty)
  dbus <- Notify.connectSession
  config <- getConfig
  case args of
    ["daemon"] ->
      daemonAction config acid dbus
    ["list"] ->
      listAction config acid
    ["test", url, selector] ->
      testAction config (pack url) (pack selector)
    ["help"] ->
      helpAction config
    _ ->
      helpAction config
  closeAcidState acid

-- TODO: make file location configurable
getConfig :: IO Config
getConfig =
  either E.throw id <$> Yaml.decodeFileEither "./config.yaml"


-- Actions

-- TODO: make this better
helpAction :: Config -> IO ()
helpAction _ = putStrLn "Help screen"

-- TODO: make this better
listAction :: Config -> AcidState WatchItState -> IO ()
listAction config acid =
  forM_ (sites config) $ \site -> do
    mSiteData <- query acid (LookupSiteData $ makeSiteKey site)
    case mSiteData of
      Nothing ->
        putStrLn $ show site ++ "not checked yet"
      Just siteData ->
        putStrLn $ show site ++ show siteData

-- TODO: make this better, support easily querying an existing site configuration
testAction :: Config -> Text -> Text -> IO ()
testAction _ url selector =
  print =<< getSiteContent Site { siteUrl = url, siteSelector = selector }

daemonAction :: Config -> AcidState WatchItState -> Notify.Client -> IO ()
daemonAction config acid dbus = forever $ do
  -- TODO: do all these in parallel/concurrently? having each site run it's own
  -- worker would also allow fairly straightforward staggering of the work,
  -- sites using the same url but different selectors should be combined in a
  -- single worker/query so we only request the same page once
  forM_ (sites config) $ \site -> do
    now <- getCurrentTime
    mSiteData <- query acid (LookupSiteData $ makeSiteKey site)
    content <- getSiteContent site
    let contentHash' = hash content
    newSiteData <- case mSiteData of
      Nothing ->
        return SiteData { lastChecked = now, lastChanged = now, contentHash = contentHash'}
      Just siteData -> do
        let siteData' = siteData { lastChecked = now } :: SiteData
        if contentHash' /= contentHash (siteData :: SiteData)
          then do
            _ <- Notify.notify dbus $ changedNote site
            return siteData' { lastChanged = now, contentHash = contentHash' }
          else
            return siteData'
    update acid (InsertSiteData (makeSiteKey site) newSiteData)

  -- Wait a bit and go again
  threadDelay ((checkFreqSec config)*1000*1000)


-- Helpers

getSiteContent :: Site -> IO ByteString
getSiteContent site = do
  mResponse <- safeGet $ unpack (siteUrl site)
  case mResponse of
    Nothing -> return ""
    Just r ->
      return $ selectContent (siteSelector site) (r ^. Wreq.responseBody)
  where
    safeGet :: String -> IO (Maybe (Wreq.Response LB.ByteString))
    safeGet url = (Just <$> Wreq.get url) `E.catch` handler
    handler :: HttpException -> IO (Maybe a)
    handler _ = return Nothing

selectContent :: Text -> LB.ByteString -> ByteString
selectContent selector' =
    encodeUtf8 . Haquery.render . Haquery.tag "dummy" [] . Haquery.select selector' . Haquery.tag "dummy" [] . Haquery.parseHtml . decodeUtf8 . LB.toStrict

changedNote :: Site -> Notify.Note
changedNote site = appNote
  { Notify.summary = "Content changed"
  , Notify.body = Just (Notify.Text (unpack $ siteUrl site))
  }

appNote :: Notify.Note
appNote = Notify.blankNote { Notify.appName = "Watch It" }
