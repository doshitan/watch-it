{ pkgs ? (import <nixpkgs> {})
, haskellPackages ? pkgs.haskell.packages.ghc801
, haskellDevTools ? (if pkgs ? myHaskellDevTools then pkgs.myHaskellDevTools else (p : []))
}:

let
  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: {
      watch-it = self.callPackage ./. {};
    };
  };

  package = with modifiedHaskellPackages;
    pkgs.haskell.lib.addBuildTools
      watch-it
      (haskellDevTools modifiedHaskellPackages ++ [cabal-install]);
in
  package.env
