{ mkDerivation, acid-state, aeson, base, bytestring, containers
, fdo-notify, haquery, hashing, http-client, lens, mtl, safecopy
, stdenv, text, time, wreq, yaml
}:
mkDerivation {
  pname = "watch-it";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    acid-state aeson base bytestring containers fdo-notify haquery
    hashing http-client lens mtl safecopy text time wreq yaml
  ];
  license = stdenv.lib.licenses.agpl3;
}
