# Watch It

A silly thing to periodically grab a web page and see if its content has
changed, creating a desktop notification if it has.

You probably want to use something like:

- [visualping](https://visualping.io/)
- [wachete](https://www.wachete.com/)
- [Versionista](https://versionista.com/)
- [Follow That Page](https://www.followthatpage.com/)
- [Distill Web Monitor](https://addons.mozilla.org/en-us/firefox/addon/alertbox/)

or any other number of better things instead of this.
